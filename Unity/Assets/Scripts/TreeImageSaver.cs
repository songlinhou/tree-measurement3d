﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ImageSampler))]
public class TreeImageSaver : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        ImageSampler imageSynthesis = (ImageSampler)target;
        int photoID = 0;

        // Only display the "Save" button if playing
        if (EditorApplication.isPlaying && GUILayout.Button("Save Captures")) 
        {
            photoID = imageSynthesis.getCurrentSavedNumber();
            Vector2 gameViewSize = Handles.GetMainGameViewSize();
            string filename = photoID + ".png"; 
            imageSynthesis.DoPositionRandomization();
            imageSynthesis.Save(filename, width: (int)gameViewSize.x, height: (int)gameViewSize.y, imageSynthesis.filepath);
        }

        if (EditorApplication.isPlaying && GUILayout.Button("Random Position")) {
            imageSynthesis.DoPositionRandomization();
        }

        //if (EditorApplication.isPlaying && GUILayout.Button("Generate Images and Masks"))
        //{
        //    Vector2 gameViewSize = Handles.GetMainGameViewSize();
        //    imageSynthesis.GenerateImagesAndMasks(width: (int)gameViewSize.x, height: (int)gameViewSize.y, imageSynthesis.filepath);
        //}
    }
}
