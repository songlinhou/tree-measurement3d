﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    public GameObject targetTrees;
    private GameObject camObject;

    public float minDistance = 2.0f;

    public Vector2 xRange = new Vector2(113f, 236f);
    public Vector2 yRange = new Vector2(11f, 24f);
    public Vector2 zRange = new Vector2(93f, 190f);


    //public Vector2 xRange = new Vector2(113f,236f);
    //public Vector2 yRange = new Vector2(11f, 24f);
    //public Vector2 zRange = new Vector2(93f, 190f);


    [SerializeField]
    private GameObject target;

    public List<Transform> targets;
    // Start is called before the first frame update
    void Start()
    {
        camObject = Camera.main.gameObject;
        Transform[] _targets = targetTrees.GetComponentsInChildren<Transform>();

        foreach (Transform t in _targets) {
            if (! t.gameObject.Equals(targetTrees)) {
                targets.Add(t);
            }
        }
        
        if (targets.Count == 0) {
            print("target should have at least one child");
        }

        SetupOnePos();
    }

    public void SetupOnePos() {
        float x, y, z;
        do {
            // get a random position
            camObject = Camera.main.gameObject;
            x = Random.Range(xRange[0], xRange[1]);
            y = Random.Range(yRange[0], yRange[1]);
            z = Random.Range(zRange[0], zRange[1]);

            camObject.transform.position = new Vector3(x, y, z);

            int choice = Random.Range(0, targets.Count - 1);
            print("choice=" + choice);
            target = targets[choice].gameObject;
            camObject.transform.LookAt(target.transform);

        }
        while (Vector2.Distance(new Vector2(x,z), new Vector2(target.transform.position.x, target.transform.position.z)) < minDistance);
        
    }

    public void ChangeTreeShape() {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
